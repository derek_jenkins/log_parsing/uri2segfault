#!/bin/bash
#  filename: uri2segfault.sh
#  use: this for loop will parse timestamps out of php-fpm error log entries where segmentation faults were recorded.
#+ the script then reformats the timestamps of those segfault entries so that it can fetch the associated records out of an nginx access log.
#  in short -- whenever a php-fpm segfault was logged, show us the related nginx access log entry.
# # # # #
PHP_FPM_LOG="/var/log/php-fpm/error.log"
NGINX_ACCESS_LOG="/var/log/nginx/access.log"
echo
for NGINX_REQUEST in \
$(for UNDERSCORE_TIMESTAMP in \
$(zgrep -h "Sep-2017.*SIGSEGV - core dumped" "${PHP_FPM_LOG}"* | cut -d "]" -f1 | cut -d "[" -f2 | sed 's/-2017 /-2017_/g'); do
    SPACE_TIMESTAMP=$(echo "${UNDERSCORE_TIMESTAMP}" | sed 's/_/ /g')
    echo "$(date -d "${SPACE_TIMESTAMP}" "+%d/%b/%Y:%H:%M:%S")"
done); do
    zgrep "${NGINX_REQUEST}" "${NGINX_ACCESS_LOG}"* # | awk '{print stuff}' # <-- leaving this here as a reminder that we could do some "awkery" at this point.
done
