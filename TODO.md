TODO:
0) remedy the hard-coding on line 12.
1) make uri2segfault.sh interactive by polling user for some info, such as "which date".
1.5) consider making this interactive/non-interactive based on invocation.
2) make this ask the user for the location of the nginx file that they want to log. alternatively, i could spec flags (like --nginx-log=). this could allow functionality for apache logs.